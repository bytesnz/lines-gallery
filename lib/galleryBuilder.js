"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.optionDefaults = void 0;
/**
 * lines-gallery option defaults
 */
exports.optionDefaults = {
    panaramaRatioMinimum: 3,
    targetImagesPerLine: 3,
    imageMargin: 10
};
exports.default = (function (images, callback, options) {
    var haveNoMetadata = false;
    var rowImages = [];
    var processedImages = [];
    var lastRowImages = null;
    var lastImage = -1;
    var firstModifiedImage = 0;
    var firstRow = true;
    options = __assign(__assign({}, exports.optionDefaults), options);
    var response = {
        images: [],
        setWidth: function (newWidth) {
            if (newWidth !== options.galleryWidth) {
                options.galleryWidth = newWidth;
                firstModifiedImage = 0;
                relayoutImages();
                callback(response.images, firstModifiedImage);
            }
        }
    };
    /**
     * Place the images that are remaining in the lastRowImages and rowImages
     */
    var addLastImages = function () {
        if (lastRowImages) {
            insertImageRow(lastRowImages);
        }
        if (rowImages) {
            insertImageRow(rowImages);
        }
    };
    /**
     * Calculate the combined width of the given images based on sizing the
     * images so they all have the same height as the image with the smallest
     * height value
     *
     * @param images Images to caculate the combined width for
     *
     * @returns The combined width
     */
    var calculateCombinedWidth = function (images) {
        if (!images.length) {
            return;
        }
        var smallestHeight = images.reduce(function (height, image) {
            return height === null ? image.height : Math.min(height, image.height);
        }, null);
        var firstHeight = images[0].height;
        return images.reduce(function (total, image) {
            image.heightFactor = smallestHeight / image.height;
            total += image.width * image.heightFactor;
            return total;
        }, 0);
    };
    /**
     * Calculate a scale factor based on the combined width and the gallery
     * width minus padding between the images
     *
     * @param images Images to calculate the scale factor for
     *
     * @returns The scale factor for the row of images
     */
    var calculateScaleFactor = function (images) {
        return (options.galleryWidth - options.imageMargin * (images.length - 1))
            / calculateCombinedWidth(images);
    };
    /**
     * Calculate the widths for the given images to fill a row
     *
     * @param images Images to insert
     */
    var insertImageRow = function (images) {
        console.log('insertImageRow called', images, firstRow);
        if (!images.length) {
            debugger;
        }
        var scaleFactor = Math.min(1, calculateScaleFactor(images));
        images.forEach(function (image, index) {
            var imageFactor = scaleFactor * image.heightFactor;
            image.clientWidth = Math.floor(image.width * imageFactor);
            image.clientHeight = Math.floor(image.height * imageFactor);
            image.imageFirstInRow = !index;
            image.firstRowImage = firstRow;
            if (response.images.indexOf(image) === -1) {
                response.images.push(image);
            }
        });
        firstRow = false;
    };
    /**
     * Put an image into a row array
     *
     * @param image Image to insert into the row
     */
    var placeImageInRow = function (image) {
        // Put panoramas on their own row if they are bigger than the galleryWidth
        if (image.width > options.galleryWidth &&
            image.width / image.height >= options.panaramaRatioMinimum) {
            if (rowImages.length === 1 && lastRowImages) {
                // Add row images to last row and print
                lastRowImages = lastRowImages.concat(rowImages);
                rowImages = [];
            }
            if (lastRowImages) {
                insertImageRow(lastRowImages);
            }
            lastRowImages = null;
            if (rowImages.length) {
                insertImageRow(rowImages);
                rowImages = [];
            }
            insertImageRow([image]);
            return;
        }
        var addedRowImages = __spreadArrays(rowImages, [image]);
        var combinedWidth = calculateCombinedWidth(addedRowImages);
        // Check if images will be smaller than the min image size if this
        // image is added to the row
        if (options.minImageSize && combinedWidth > options.galleryWidth) {
            var scaleFactor_1 = calculateScaleFactor(addedRowImages);
            if (scaleFactor_1 < 1) {
                var index = addedRowImages.findIndex(function (image) {
                    if (image.width * image.heightFactor > options.minImageSize
                        || image.height * image.heightFactor) {
                        // Check if with the scale factor, the image will be smaller
                        // than the minImageSize
                        if (image.width * image.heightFactor * scaleFactor_1 < options.minImageSize
                            || image.height * image.heightFactor * scaleFactor_1 < options.minImageSize) {
                            return true;
                        }
                    }
                });
                if (index !== -1) {
                    if (lastRowImages) {
                        insertImageRow(lastRowImages);
                    }
                    lastRowImages = rowImages;
                    rowImages = [image];
                    return;
                }
            }
        }
        // Add image to row
        rowImages = addedRowImages;
        if (rowImages.length >= options.targetImagesPerLine) {
            if (combinedWidth >= options.galleryWidth) {
                if (lastRowImages) {
                    insertImageRow(lastRowImages);
                }
                lastRowImages = rowImages;
                rowImages = [];
            }
        }
    };
    var addLoadedImages = function () {
        var index = lastImage + 1;
        while (index < processedImages.length) {
            var image = processedImages[index];
            if (image.width && image.height) {
                placeImageInRow(image);
                lastImage = index;
                // Add last images to gallery if complete
                if (index === processedImages.length - 1) {
                    haveNoMetadata = false;
                    // Add last images and then relayout incase have to take into account scrollbars
                    addLastImages();
                    relayoutImages();
                }
            }
            else if (!image.errored) {
                break;
            }
            index++;
        }
    };
    var relayoutImages = function () {
        lastRowImages = null;
        rowImages = [];
        firstRow = true;
        processedImages.find(function (image) {
            if (typeof image === 'string' || (typeof image === 'object'
                && (!image.width || !image.height))) {
                return true;
            }
            placeImageInRow(image);
        });
        if (!haveNoMetadata) {
            addLastImages();
        }
        callback(response.images, firstModifiedImage);
    };
    images.forEach(function (givenImage, index) {
        var image;
        if (typeof givenImage === 'string' || (typeof givenImage === 'object'
            && (!givenImage.width || !givenImage.height))) {
            haveNoMetadata = true;
        }
        if (!haveNoMetadata) {
            image = __assign({}, givenImage);
            processedImages[index] = image;
            placeImageInRow(processedImages[index]);
            lastImage = index;
        }
        else {
            if (typeof givenImage === 'string') {
                image = {
                    src: givenImage,
                };
                processedImages[index] = image;
            }
            // Skip images if we have sized image request and image metadata
            if (options.sizedImageRequest && image.width && image.height) {
                return;
            }
            // Create img element for loading the image
            var imageElement_1 = document.createElement('img');
            imageElement_1.src = image.src;
            imageElement_1.addEventListener('load', function () {
                image.loaded = true;
                image.width = imageElement_1.naturalWidth;
                image.height = imageElement_1.naturalHeight;
                image.element = imageElement_1;
                delete image.element;
                addLoadedImages();
            });
            imageElement_1.addEventListener('error', function () {
                image.errored = true;
                delete image.element;
                addLoadedImages();
            });
            image.element = imageElement_1;
        }
    });
    // Row the last images
    if (!haveNoMetadata) {
        // Add last images and then relayout incase have to take into account scrollbars
        addLastImages();
        relayoutImages();
    }
    else {
        callback(response.images, firstModifiedImage);
    }
    return response;
});

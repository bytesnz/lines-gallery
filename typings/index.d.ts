/**
 * Options that can be passed to linesGallery
 *
 * @see optionDefaults for the default values
 */
export interface Options {
  /// Minimum width/height ratio to detect as panaramas
  panaramaRatioMinimum?: number,
  /// Width of gallery
  galleryWidth?: number,
  /// Target number of images per line
  targetImagesPerLine?: number,
  /// Margin between images in pixels
  imageMargin?: number,
  /// Target minimum size of resized images. If an image is larger than this
  /// but will be resized to small than this in either dimension, the number
  /// of images on the row will be adjusted to try and keep the image size
  /// above this value
  minImageSize?: number,
  /// Watch for resize events to resize images when required
  resizeImages?: boolean,
  /**
   * Image URL modification function. Will be called to modify the urls of
   * images before the request is sent to the server
   *
   * @param url Image URL
   * @param width Wanted image width, if have image metadata
   * @param height Wanted image height, if have image metadata
   *
   * @returns Modified URL
   */
  sizedImageRequest? (url: string, width?: number, height?: number): string
}

export interface FilledOptions extends Options {
  galleryWidth: number
}

/**
 * Image metadata to speed up sizing of images and to request an image of a
 * particular size if enabled
 */
export interface ImageMetadata {
  /// URL of image
  src: string,
  /// Image width in pixels
  width?: number,
  /// Image height in pixels
  height?: number
}

/**@internal
 * Additional data stored on processed images to make rendering and resizing
 * faster
 */
export interface ProcessedImage extends ImageMetadata {
  /// Whether or not the image loading has errored
  errored?: boolean,
  /// Created image element
  element?: HTMLElement,
  /// Whether or not the image has loaded
  loaded?: boolean,
  /// Factor image should be scaled by to match row height
  heightFactor?: number,
  /// Width image should be when displayed. Only set when image has loaded
  clientWidth?: number,
  /// Height image should be when displayed. Only set when image has loaded
  clientHeight?: number,
  /// Whether or not the image is the first image in the row
  imageFirstInRow?: boolean,
  /// Whether or not the image is in the first row
  firstRowImage?: boolean
}

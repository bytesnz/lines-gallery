import { terser } from "rollup-plugin-terser";
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import typescript from 'rollup-plugin-typescript';

export default [
  {
    input: 'src/linesGallery.ts',
    output: {
      file: 'linesGallery.js',
      format: 'iife'
    },
    plugins: [
      resolve(),
      commonjs(),
      typescript()
    ]
  }
];

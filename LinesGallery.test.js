"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ava_1 = __importDefault(require("ava"));
var react_1 = __importDefault(require("react"));
var LinesGallery_1 = __importDefault(require("./LinesGallery"));
var react_test_renderer_1 = __importDefault(require("react-test-renderer"));
var mocks_1 = require("./tests/helpers/mocks");
var testOptions = {
    galleryWidth: 1000
};
ava_1.default('renders images instantly when given metadata, with a galleryWidth option', function (t) {
    var testRenderer = react_test_renderer_1.default.create(react_1.default.createElement(LinesGallery_1.default, __assign({ images: mocks_1.imagesWithMetadata }, testOptions)));
    var testInstance = testRenderer.root;
    var imgs = testInstance.findAllByType('img');
    t.is(mocks_1.imagesWithMetadata.length, imgs.length, 'correct amount of images');
});
ava_1.default('renders images as good as instantly when given metadata', function (t) {
    var testRenderer = react_test_renderer_1.default.create(react_1.default.createElement(LinesGallery_1.default, { images: mocks_1.imagesWithMetadata }), {
        createNodeMock: function (element) {
            if (element.type === 'div') {
                return {
                    clientWidth: 1000
                };
            }
        }
    });
    var testInstance = testRenderer.root;
    var imgs = testInstance.findAllByType('img');
    t.is(mocks_1.imagesWithMetadata.length, imgs.length, 'correct amount of images');
    t.is('img1.png', imgs[0].props.src, "image 1 src is not correct");
    t.is(310, imgs[0].props.width, "image 1 width not correct");
    t.is(232, imgs[0].props.height, "image 1 height not correct");
    t.is('img2.png', imgs[1].props.src, "image 2 src is not correct");
    t.is(174, imgs[1].props.width, "image 2 width not correct");
    t.is(232, imgs[1].props.height, "image 2 height not correct");
    t.is('img3.png', imgs[2].props.src, "image 3 src is not correct");
    t.is(310, imgs[2].props.width, "image 3 width not correct");
    t.is(232, imgs[2].props.height, "image 3 height not correct");
    t.is('img4.png', imgs[3].props.src, "image 4 src is not correct");
    t.is(174, imgs[3].props.width, "image 4 width not correct");
    t.is(232, imgs[3].props.height, "image 4 height not correct");
    t.is('img5.png', imgs[4].props.src, "image 5 src is not correct");
    t.is(1000, imgs[4].props.width, "image 5 width not correct");
    t.is(320, imgs[4].props.height, "image 5 height not correct");
    t.is('img6.png', imgs[5].props.src, "image 6 src is not correct");
    t.is(633, imgs[5].props.width, "image 6 width not correct");
    t.is(475, imgs[5].props.height, "image 6 height not correct");
    t.is('img7.png', imgs[6].props.src, "image 7 src is not correct");
    t.is(356, imgs[6].props.width, "image 7 width not correct");
    t.is(475, imgs[6].props.height, "image 7 height not correct");
});

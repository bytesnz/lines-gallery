"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const galleryBuilder_1 = __importDefault(require("./lib/galleryBuilder"));
class LinesGallery extends react_1.default.Component {
    constructor(props) {
        super(props);
        this.state = {
            readyImages: []
        };
        this.mounted = false;
        this.galleryOptions = {
            galleryWidth: null
        };
        [
            'panaramaRatioMinimum',
            'imageMargin',
            'minImageSize',
            'resizeImages',
            'sizedImageRequest'
        ].forEach((key) => {
            if (typeof props[key] !== 'undefined') {
                this.galleryOptions[key] = props[key];
            }
        });
        this.div = react_1.default.createRef();
        if (props.galleryWidth) {
            this.galleryOptions.galleryWidth = props.galleryWidth;
            const response = galleryBuilder_1.default(this.props.images, this.updateReadyImages.bind(this), this.galleryOptions);
            this.setWidth = response.setWidth;
            this.state.readyImages = response.images;
            this.state.width = props.galleryWidth;
        }
        this.resize = this.resize.bind(this);
    }
    componentDidMount() {
        this.mounted = true;
        if (!this.props.galleryWidth) {
            const width = this.div.current.clientWidth;
            this.galleryOptions.galleryWidth = width;
            const response = galleryBuilder_1.default(this.props.images, this.updateReadyImages.bind(this), this.galleryOptions);
            this.setWidth = response.setWidth;
            this.setState({
                width,
                readyImages: response.images
            });
            window.addEventListener('resize', this.resize);
        }
    }
    resize() {
        if (this.setWidth && this.div.current) {
            const width = this.div.current.clientWidth;
            this.setState({
                width
            });
            this.setWidth(width);
        }
    }
    componentWillUnmount() {
        this.mounted = false;
        if (!this.props.galleryWidth) {
            window.removeEventListener('resize', this.resize);
        }
    }
    updateReadyImages(images) {
        if (this.mounted) {
            this.setState({
                readyImages: images
            });
        }
        else {
            this.state.readyImages = images;
        }
    }
    render() {
        return (react_1.default.createElement("div", { ref: this.div, style: { overflowX: 'hidden' } },
            react_1.default.createElement("div", { style: { width: this.state.width && `${this.state.width}px` } }, this.state.readyImages.map((image, index) => (react_1.default.createElement("img", { src: image.src, key: index, width: image.clientWidth, height: image.clientHeight, style: {
                    marginLeft: !image.firstRowImage && this.props.imageMargin && `${this.props.imageMargin}px`,
                    marginBottom: this.props.imageMargin && `${this.props.imageMargin}px`
                } }))))));
    }
}
exports.default = LinesGallery;

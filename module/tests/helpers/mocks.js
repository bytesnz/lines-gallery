"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.imagesWithoutMetadata = exports.imagesWithMetadata = void 0;
exports.imagesWithMetadata = [
    {
        width: 640,
        height: 480,
        src: 'img1.png'
    },
    {
        width: 480,
        height: 640,
        src: 'img2.png'
    },
    {
        width: 640,
        height: 480,
        src: 'img3.png'
    },
    {
        width: 480,
        height: 640,
        src: 'img4.png'
    },
    {
        width: 1500,
        height: 480,
        src: 'img5.png'
    },
    {
        width: 640,
        height: 480,
        src: 'img6.png'
    },
    {
        width: 480,
        height: 640,
        src: 'img7.png'
    }
];
exports.imagesWithoutMetadata = [
    'img1.png',
    'img2.png',
    'img3.png',
    'img4.png',
    'img5.png',
    'img6.png',
    'img7.png'
];

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.optionDefaults = void 0;
/**
 * lines-gallery option defaults
 */
exports.optionDefaults = {
    panaramaRatioMinimum: 3,
    targetImagesPerLine: 3,
    imageMargin: 10
};
exports.default = (images, callback, options) => {
    let haveNoMetadata = false;
    let rowImages = [];
    let processedImages = [];
    let lastRowImages = null;
    let lastImage = -1;
    let firstModifiedImage = 0;
    let firstRow = true;
    options = Object.assign(Object.assign({}, exports.optionDefaults), options);
    const response = {
        images: [],
        setWidth: (newWidth) => {
            if (newWidth !== options.galleryWidth) {
                options.galleryWidth = newWidth;
                firstModifiedImage = 0;
                relayoutImages();
                callback(response.images, firstModifiedImage);
            }
        }
    };
    /**
     * Place the images that are remaining in the lastRowImages and rowImages
     */
    const addLastImages = () => {
        if (lastRowImages) {
            insertImageRow(lastRowImages);
        }
        if (rowImages) {
            insertImageRow(rowImages);
        }
    };
    /**
     * Calculate the combined width of the given images based on sizing the
     * images so they all have the same height as the image with the smallest
     * height value
     *
     * @param images Images to caculate the combined width for
     *
     * @returns The combined width
     */
    const calculateCombinedWidth = (images) => {
        if (!images.length) {
            return;
        }
        const smallestHeight = images.reduce((height, image) => height === null ? image.height : Math.min(height, image.height), null);
        const firstHeight = images[0].height;
        return images.reduce((total, image) => {
            image.heightFactor = smallestHeight / image.height;
            total += image.width * image.heightFactor;
            return total;
        }, 0);
    };
    /**
     * Calculate a scale factor based on the combined width and the gallery
     * width minus padding between the images
     *
     * @param images Images to calculate the scale factor for
     *
     * @returns The scale factor for the row of images
     */
    const calculateScaleFactor = (images) => (options.galleryWidth - options.imageMargin * (images.length - 1))
        / calculateCombinedWidth(images);
    /**
     * Calculate the widths for the given images to fill a row
     *
     * @param images Images to insert
     */
    const insertImageRow = (images) => {
        const scaleFactor = Math.min(1, calculateScaleFactor(images));
        images.forEach((image, index) => {
            const imageFactor = scaleFactor * image.heightFactor;
            image.clientWidth = Math.floor(image.width * imageFactor);
            image.clientHeight = Math.floor(image.height * imageFactor);
            image.imageFirstInRow = !index;
            image.firstRowImage = firstRow;
            if (response.images.indexOf(image) === -1) {
                response.images.push(image);
            }
        });
        firstRow = false;
    };
    /**
     * Put an image into a row array
     *
     * @param image Image to insert into the row
     */
    const placeImageInRow = (image) => {
        // Put panoramas on their own row if they are bigger than the galleryWidth
        if (image.width > options.galleryWidth &&
            image.width / image.height >= options.panaramaRatioMinimum) {
            if (rowImages.length === 1 && lastRowImages) {
                // Add row images to last row and print
                lastRowImages = lastRowImages.concat(rowImages);
                rowImages = [];
            }
            if (lastRowImages) {
                insertImageRow(lastRowImages);
            }
            lastRowImages = null;
            if (rowImages.length) {
                insertImageRow(rowImages);
                rowImages = [];
            }
            insertImageRow([image]);
            return;
        }
        const addedRowImages = [...rowImages, image];
        const combinedWidth = calculateCombinedWidth(addedRowImages);
        // Check if images will be smaller than the min image size if this
        // image is added to the row
        if (options.minImageSize && combinedWidth > options.galleryWidth) {
            const scaleFactor = calculateScaleFactor(addedRowImages);
            if (scaleFactor < 1) {
                const index = addedRowImages.findIndex((image) => {
                    if (image.width * image.heightFactor > options.minImageSize
                        || image.height * image.heightFactor) {
                        // Check if with the scale factor, the image will be smaller
                        // than the minImageSize
                        if (image.width * image.heightFactor * scaleFactor < options.minImageSize
                            || image.height * image.heightFactor * scaleFactor < options.minImageSize) {
                            return true;
                        }
                    }
                });
                if (index !== -1) {
                    if (lastRowImages) {
                        insertImageRow(lastRowImages);
                    }
                    lastRowImages = rowImages;
                    rowImages = [image];
                    return;
                }
            }
        }
        // Add image to row
        rowImages = addedRowImages;
        if (rowImages.length >= options.targetImagesPerLine) {
            if (combinedWidth >= options.galleryWidth) {
                if (lastRowImages) {
                    insertImageRow(lastRowImages);
                }
                lastRowImages = rowImages;
                rowImages = [];
            }
        }
    };
    const addLoadedImages = () => {
        let index = lastImage + 1;
        while (index < processedImages.length) {
            const image = processedImages[index];
            if (image.width && image.height) {
                placeImageInRow(image);
                lastImage = index;
                // Add last images to gallery if complete
                if (index === processedImages.length - 1) {
                    haveNoMetadata = false;
                    // Add last images and then relayout incase have to take into account scrollbars
                    addLastImages();
                    relayoutImages();
                }
            }
            else if (!image.errored) {
                break;
            }
            index++;
        }
    };
    const relayoutImages = () => {
        lastRowImages = null;
        rowImages = [];
        firstRow = true;
        processedImages.find((image) => {
            if (typeof image === 'string' || (typeof image === 'object'
                && (!image.width || !image.height))) {
                return true;
            }
            placeImageInRow(image);
        });
        if (!haveNoMetadata) {
            addLastImages();
        }
        callback(response.images, firstModifiedImage);
    };
    images.forEach((givenImage, index) => {
        let image;
        if (typeof givenImage === 'string' || (typeof givenImage === 'object'
            && (!givenImage.width || !givenImage.height))) {
            haveNoMetadata = true;
        }
        if (!haveNoMetadata) {
            image = Object.assign({}, givenImage);
            processedImages[index] = image;
            placeImageInRow(processedImages[index]);
            lastImage = index;
        }
        else {
            if (typeof givenImage === 'string') {
                image = {
                    src: givenImage,
                };
                processedImages[index] = image;
            }
            // Skip images if we have sized image request and image metadata
            if (options.sizedImageRequest && image.width && image.height) {
                return;
            }
            // Create img element for loading the image
            let imageElement = document.createElement('img');
            imageElement.src = image.src;
            imageElement.addEventListener('load', () => {
                image.loaded = true;
                image.width = imageElement.naturalWidth;
                image.height = imageElement.naturalHeight;
                image.element = imageElement;
                delete image.element;
                addLoadedImages();
            });
            imageElement.addEventListener('error', () => {
                image.errored = true;
                delete image.element;
                addLoadedImages();
            });
            image.element = imageElement;
        }
    });
    // Row the last images
    if (!haveNoMetadata) {
        // Add last images and then relayout incase have to take into account scrollbars
        addLastImages();
        relayoutImages();
    }
    else {
        callback(response.images, firstModifiedImage);
    }
    return response;
};

import { linesGallery } from './index';

declare global {
  interface Window {
    linesGallery: any;
  }
}

window.linesGallery = linesGallery;

import test from 'ava';
import { imagesWithoutMetadata, imagesWithMetadata } from './tests/helpers/mocks'

import linesGallery from './index';
import { optionDefaults } from './lib/galleryBuilder';

const testOptions = {
  galleryWidth: 1000
};

test('linesGallery() creates images in the given div instantly when given metadata', (t) => {
  const testDiv = document.createElement('div');

  linesGallery(testDiv, imagesWithMetadata, testOptions);

  const imageElements = testDiv.getElementsByTagName('img');

  t.is(imagesWithMetadata.length, imageElements.length);
  t.is('img1.png', imageElements[0].src, `image 1 src is not correct`);
  t.is(310, imageElements[0].width, `image 1 width not correct`);
  t.is(232, imageElements[0].height, `image 1 height not correct`);
  t.is('img2.png', imageElements[1].src, `image 2 src is not correct`);
  t.is(174, imageElements[1].width, `image 2 width not correct`);
  t.is(232, imageElements[1].height, `image 2 height not correct`);
  t.is('img3.png', imageElements[2].src, `image 3 src is not correct`);
  t.is(310, imageElements[2].width, `image 3 width not correct`);
  t.is(232, imageElements[2].height, `image 3 height not correct`);
  t.is('img4.png', imageElements[3].src, `image 4 src is not correct`);
  t.is(174, imageElements[3].width, `image 4 width not correct`);
  t.is(232, imageElements[3].height, `image 4 height not correct`);
  t.is('img5.png', imageElements[4].src, `image 5 src is not correct`);
  t.is(1000, imageElements[4].width, `image 5 width not correct`);
  t.is(320, imageElements[4].height, `image 5 height not correct`);
  t.is('img6.png', imageElements[5].src, `image 6 src is not correct`);
  t.is(633, imageElements[5].width, `image 6 width not correct`);
  t.is(475, imageElements[5].height, `image 6 height not correct`);
  t.is('img7.png', imageElements[6].src, `image 7 src is not correct`);
  t.is(356, imageElements[6].width, `image 7 width not correct`);
  t.is(475, imageElements[6].height, `image 7 height not correct`);
});

test('linesGallery() does not create images in div instantly if not metadata', (t) => {
  const testDiv = document.createElement('div');

  linesGallery(testDiv, imagesWithoutMetadata, testOptions);

  t.is('', testDiv.innerHTML, 'HTML of div does not match what it should');
});

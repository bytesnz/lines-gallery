import { ProcessedImage, Options, FilledOptions, ImageMetadata } from '../typings/index';
import React from 'react';

import galleryBuilder from './lib/galleryBuilder';

interface LinesGalleryState {
  readyImages: Array<ProcessedImage>,
  width?: number
}

interface LinesGalleryProps extends Options {
  images: Array<string | ImageMetadata>
}

export default class LinesGallery extends React.Component<LinesGalleryProps,
    LinesGalleryState> {
  state: LinesGalleryState = {
    readyImages: []
  };
  protected setWidth: (width: number) => void;
  protected div;
  protected mounted: boolean = false;
  protected resizeDebounce: number;
  protected galleryOptions: FilledOptions;

  constructor (props) {
    super(props);

    this.galleryOptions = {
      galleryWidth: null
    };
    [
      'panaramaRatioMinimum',
      'imageMargin',
      'minImageSize',
      'resizeImages',
      'sizedImageRequest'
    ].forEach((key) => {
      if (typeof props[key] !== 'undefined') {
        this.galleryOptions[key] = props[key];
      }
    });

    this.div = React.createRef();
    if (props.galleryWidth) {
      this.galleryOptions.galleryWidth = props.galleryWidth;
      const response = galleryBuilder(this.props.images, this.updateReadyImages.bind(this),
        this.galleryOptions);
      this.setWidth = response.setWidth;
      this.state.readyImages = response.images;
      this.state.width = props.galleryWidth;
    }

    this.resize = this.resize.bind(this);
  }

  componentDidMount () {
    this.mounted = true;
    if (!this.props.galleryWidth) {
      const width = this.div.current.clientWidth;
      this.galleryOptions.galleryWidth = width;

      const response = galleryBuilder(this.props.images, this.updateReadyImages.bind(this),
        this.galleryOptions);
      this.setWidth = response.setWidth;
      this.setState({
        width,
        readyImages: response.images
      });

      window.addEventListener('resize', this.resize);
    }
  }

  resize () {
    if (this.setWidth && this.div.current) {
      const width = this.div.current.clientWidth;
      this.setState({
        width
      });
      this.setWidth(width);
    }
  }

  componentWillUnmount () {
    this.mounted = false;

    if (!this.props.galleryWidth) {
      window.removeEventListener('resize', this.resize);
    }
  }

  updateReadyImages (images) {
    if (this.mounted) {
      this.setState({
        readyImages: images
      });
    } else {
      this.state.readyImages = images;
    }
  }

  render () {
    return (<div ref={this.div} style={{ overflowX: 'hidden' }}>
      <div style={{ width: this.state.width && `${this.state.width}px` }}>
        {this.state.readyImages.map((image, index) => (
          <img src={image.src} key={index} width={image.clientWidth} height={image.clientHeight} style={{
            marginLeft: !image.firstRowImage && this.props.imageMargin && `${this.props.imageMargin}px`,
            marginBottom: this.props.imageMargin && `${this.props.imageMargin}px`
          }} />
        )) }
      </div>
    </div>);
  }
}

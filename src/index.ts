import { ImageMetadata, ProcessedImage, Options, FilledOptions } from '../typings/index';

import galleryBuilder from './lib/galleryBuilder';

/**
 * Create a lines gallery in the given element using the given images
 *
 * @param galeryElement HTML element to put the gallery in
 * @param images Image to put in the gallery
 * @param options Options
 */
export const linesGallery = (galleryElement: HTMLElement,
    images?: Array<string | ImageMetadata>, givenOptions: Options = {}) => {
  let galleryWidth = givenOptions.galleryWidth || galleryElement.clientWidth;
  const noGalleryWidth = !givenOptions.galleryWidth;

  // Add default values to options
  const options: FilledOptions = {
    ...givenOptions,
    galleryWidth
  };

  if (window.getComputedStyle(galleryElement).overflowX !== 'hidden') {
    galleryElement.style.overflowX = 'hidden';
  }

  if (window.getComputedStyle(galleryElement).lineHeight !== '0') {
    galleryElement.style.lineHeight = '0';
  }

  let elements: Array<HTMLImageElement> = [];

  const makeImageUrl = (image: ProcessedImage) => {
    if (options.sizedImageRequest) {
      return options.sizedImageRequest(image.src, image.clientWidth, image.clientHeight);
    } else {
      return image.src
    }
  };

  const updateImages = (images: Array<ProcessedImage>, firstModifiedImage: number) => {
    let i;

    // Remove any images not in images anymore
    if (elements.length > images.length) {
      for (i = images.length; i < elements.length; i++) {
      }
    }
    // Update any images that have been modified
    for (i = 0; i < elements.length; i++) {
      const src = makeImageUrl(images[i]);
      if (elements[i].src !== src) {
        elements[i].src = src;
      }

      elements[i].width = images[i].clientWidth;
      elements[i].height = images[i].clientHeight;
      if (options.imageMargin) {
        if (images[i].firstRowImage) {
          elements[i].style.marginTop = ''
        } else {
          elements[i].style.marginTop = `${options.imageMargin}px`;
        }
        if (images[i].imageFirstInRow) {
          elements[i].style.marginLeft = '';
        } else {
          elements[i].style.marginLeft = `${options.imageMargin}px`;
        }
      }
    }

    // Add new images
    for (i; i < images.length; i++) {
      const element = document.createElement('img');

      element.src = makeImageUrl(images[i]);
      element.width = images[i].clientWidth;
      element.height = images[i].clientHeight;
      if (options.imageMargin) {
        if (images[i].firstRowImage) {
          element.style.marginTop = ''
        } else {
          element.style.marginTop = `${options.imageMargin}px`;
        }
        if (images[i].imageFirstInRow) {
          element.style.marginLeft = '';
        } else {
          element.style.marginLeft = `${options.imageMargin}px`;
        }
      }

      galleryElement.appendChild(element);
      elements.push(element);
    }
  };

  if (!images) {
    // Find images already in element
    images = [];

    elements = galleryElement.querySelectorAll('img') as any;

    if (!elements.length) {
      // No images - do nothing
      return;
    }

    for (let i = 0; i < elements.length; i++) {
      images.push({
        src: elements[i].src,
        width: elements[i].width,
        height: elements[i].height
      });
    }
  }

  const response = galleryBuilder(images, updateImages, options);

  if (options.resizeImages && noGalleryWidth) {
    window.addEventListener('resize', () => {
      if (galleryElement.clientWidth !== galleryWidth) {
        galleryWidth = galleryElement.clientWidth;
        response.setWidth(galleryWidth);
      }
    });
  }
};

export default linesGallery;

import test from 'ava';
import { imagesWithoutMetadata, imagesWithMetadata } from '../tests/helpers/mocks';

import galleryBuilder from './galleryBuilder';

test('galleryBuilder gives images instantly when given with metadata', (t) => {
  const gallery = galleryBuilder(imagesWithMetadata, () => {}, {
    galleryWidth: 1000
  });
  const images = gallery.images;

  t.is('img1.png', images[0].src, `image 1 src is not correct`);
  t.is(310, images[0].clientWidth, `image 1 width not correct`);
  t.is(232, images[0].clientHeight, `image 1 height not correct`);
  t.truthy(images[0].imageFirstInRow, 'image 1 is first image in row');
  t.truthy(images[0].firstRowImage, 'image 1 is in first row');
  t.is('img2.png', images[1].src, `image 2 src is not correct`);
  t.is(174, images[1].clientWidth, `image 2 width not correct`);
  t.is(232, images[1].clientHeight, `image 2 height not correct`);
  t.falsy(images[1].imageFirstInRow, 'image 2 is not first image in row');
  t.truthy(images[1].firstRowImage, 'image 2 is first row');
  t.is('img3.png', images[2].src, `image 3 src is not correct`);
  t.is(310, images[2].clientWidth, `image 3 width not correct`);
  t.is(232, images[2].clientHeight, `image 3 height not correct`);
  t.truthy(images[0].imageFirstInRow, 'image 1 is first image in row');
  t.truthy(images[0].firstRowImage, 'image 1 is first row');
  t.is('img4.png', images[3].src, `image 4 src is not correct`);
  t.is(174, images[3].clientWidth, `image 4 width not correct`);
  t.is(232, images[3].clientHeight, `image 4 height not correct`);
  t.truthy(images[0].imageFirstInRow, 'image 1 is first image in row');
  t.truthy(images[0].firstRowImage, 'image 1 is first row');
  t.is('img5.png', images[4].src, `image 5 src is not correct`);
  t.is(1000, images[4].clientWidth, `image 5 width not correct`);
  t.is(320, images[4].clientHeight, `image 5 height not correct`);
  t.truthy(images[4].imageFirstInRow, 'image 5 is first image in row');
  t.falsy(images[4].firstRowImage, 'image 5 is not in first row');
  t.is('img6.png', images[5].src, `image 6 src is not correct`);
  t.is(633, images[5].clientWidth, `image 6 width not correct`);
  t.is(475, images[5].clientHeight, `image 6 height not correct`);
  t.truthy(images[5].imageFirstInRow, 'image 6 is first image in row');
  t.falsy(images[5].firstRowImage, 'image 6 is not in first row');
  t.is('img7.png', images[6].src, `image 7 src is not correct`);
  t.is(356, images[6].clientWidth, `image 7 width not correct`);
  t.is(475, images[6].clientHeight, `image 7 height not correct`);
  t.falsy(images[6].imageFirstInRow, 'image 7 is not first image in row');
  t.falsy(images[6].firstRowImage, 'image 7 is not in first row');
});

test('galleryBuilder limits the number of images in a row when minImageSize set', (t) => {
  const gallery = galleryBuilder(imagesWithMetadata, () => {}, {
    galleryWidth: 1000,
    minImageSize: 150,
    targetImagesPerLine: 10
  });
  const images = gallery.images;

  t.is('img1.png', images[0].src, `image 1 src is not correct`);
  t.is(310, images[0].clientWidth, `image 1 width not correct`);
  t.is(232, images[0].clientHeight, `image 1 height not correct`);
  t.truthy(images[0].imageFirstInRow, 'image 1 is first image in row');
  t.is('img2.png', images[1].src, `image 2 src is not correct`);
  t.is(174, images[1].clientWidth, `image 2 width not correct`);
  t.is(232, images[1].clientHeight, `image 2 height not correct`);
  t.is('img3.png', images[2].src, `image 3 src is not correct`);
  t.is(310, images[2].clientWidth, `image 3 width not correct`);
  t.is(232, images[2].clientHeight, `image 3 height not correct`);
  t.is('img4.png', images[3].src, `image 4 src is not correct`);
  t.is(174, images[3].clientWidth, `image 4 width not correct`);
  t.is(232, images[3].clientHeight, `image 4 height not correct`);
  t.is('img5.png', images[4].src, `image 5 src is not correct`);
  t.is(1000, images[4].clientWidth, `image 5 width not correct`);
  t.is(320, images[4].clientHeight, `image 5 height not correct`);
  t.truthy(images[4].imageFirstInRow, 'image 5 is first image in row');
  t.is('img6.png', images[5].src, `image 6 src is not correct`);
  t.is(633, images[5].clientWidth, `image 6 width not correct`);
  t.is(475, images[5].clientHeight, `image 6 height not correct`);
  t.truthy(images[5].imageFirstInRow, 'image 6 is first image in row');
  t.is('img7.png', images[6].src, `image 7 src is not correct`);
  t.is(356, images[6].clientWidth, `image 7 width not correct`);
  t.is(475, images[6].clientHeight, `image 7 height not correct`);
});

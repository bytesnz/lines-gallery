import test from 'ava';
import React from 'react';
import LinesGallery from './LinesGallery';
import TestRenderer from 'react-test-renderer';
import { imagesWithoutMetadata, imagesWithMetadata } from './tests/helpers/mocks';

const testOptions = {
  galleryWidth: 1000
};

test('renders images instantly when given metadata, with a galleryWidth option', (t) => {
  const testRenderer = TestRenderer.create(<LinesGallery images={imagesWithMetadata} {...testOptions} />);
  const testInstance = testRenderer.root;
  const imgs = testInstance.findAllByType('img');

  t.is(imagesWithMetadata.length, imgs.length, 'correct amount of images');
});

test('renders images as good as instantly when given metadata', (t) => {
  const testRenderer = TestRenderer.create(<LinesGallery images={imagesWithMetadata} />, {
    createNodeMock: (element) => {
      if (element.type === 'div') {
        return {
          clientWidth: 1000
        };
      }
    }
  });

  const testInstance = testRenderer.root;
  const imgs = testInstance.findAllByType('img');

  t.is(imagesWithMetadata.length, imgs.length, 'correct amount of images');

  t.is('img1.png', imgs[0].props.src, `image 1 src is not correct`);
  t.is(310, imgs[0].props.width, `image 1 width not correct`);
  t.is(232, imgs[0].props.height, `image 1 height not correct`);
  t.is('img2.png', imgs[1].props.src, `image 2 src is not correct`);
  t.is(174, imgs[1].props.width, `image 2 width not correct`);
  t.is(232, imgs[1].props.height, `image 2 height not correct`);
  t.is('img3.png', imgs[2].props.src, `image 3 src is not correct`);
  t.is(310, imgs[2].props.width, `image 3 width not correct`);
  t.is(232, imgs[2].props.height, `image 3 height not correct`);
  t.is('img4.png', imgs[3].props.src, `image 4 src is not correct`);
  t.is(174, imgs[3].props.width, `image 4 width not correct`);
  t.is(232, imgs[3].props.height, `image 4 height not correct`);
  t.is('img5.png', imgs[4].props.src, `image 5 src is not correct`);
  t.is(1000, imgs[4].props.width, `image 5 width not correct`);
  t.is(320, imgs[4].props.height, `image 5 height not correct`);
  t.is('img6.png', imgs[5].props.src, `image 6 src is not correct`);
  t.is(633, imgs[5].props.width, `image 6 width not correct`);
  t.is(475, imgs[5].props.height, `image 6 height not correct`);
  t.is('img7.png', imgs[6].props.src, `image 7 src is not correct`);
  t.is(356, imgs[6].props.width, `image 7 width not correct`);
  t.is(475, imgs[6].props.height, `image 7 height not correct`);
});

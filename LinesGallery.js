"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var galleryBuilder_1 = __importDefault(require("./lib/galleryBuilder"));
var LinesGallery = /** @class */ (function (_super) {
    __extends(LinesGallery, _super);
    function LinesGallery(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            readyImages: []
        };
        _this.mounted = false;
        _this.galleryOptions = {
            galleryWidth: null
        };
        [
            'panaramaRatioMinimum',
            'imageMargin',
            'minImageSize',
            'resizeImages',
            'sizedImageRequest'
        ].forEach(function (key) {
            if (typeof props[key] !== 'undefined') {
                _this.galleryOptions[key] = props[key];
            }
        });
        _this.div = react_1.default.createRef();
        if (props.galleryWidth) {
            _this.galleryOptions.galleryWidth = props.galleryWidth;
            var response = galleryBuilder_1.default(_this.props.images, _this.updateReadyImages.bind(_this), _this.galleryOptions);
            _this.setWidth = response.setWidth;
            _this.state.readyImages = response.images;
            _this.state.width = props.galleryWidth;
        }
        _this.resize = _this.resize.bind(_this);
        return _this;
    }
    LinesGallery.prototype.componentDidMount = function () {
        this.mounted = true;
        if (!this.props.galleryWidth) {
            var width = this.div.current.clientWidth;
            this.galleryOptions.galleryWidth = width;
            var response = galleryBuilder_1.default(this.props.images, this.updateReadyImages.bind(this), this.galleryOptions);
            this.setWidth = response.setWidth;
            this.setState({
                width: width,
                readyImages: response.images
            });
            window.addEventListener('resize', this.resize);
        }
    };
    LinesGallery.prototype.resize = function () {
        if (this.setWidth && this.div.current) {
            var width = this.div.current.clientWidth;
            this.setState({
                width: width
            });
            this.setWidth(width);
        }
    };
    LinesGallery.prototype.componentWillUnmount = function () {
        this.mounted = false;
        if (!this.props.galleryWidth) {
            window.removeEventListener('resize', this.resize);
        }
    };
    LinesGallery.prototype.updateReadyImages = function (images) {
        if (this.mounted) {
            this.setState({
                readyImages: images
            });
        }
        else {
            this.state.readyImages = images;
        }
    };
    LinesGallery.prototype.render = function () {
        var _this = this;
        return (react_1.default.createElement("div", { ref: this.div, style: { overflowX: 'hidden' } },
            react_1.default.createElement("div", { style: { width: this.state.width && this.state.width + "px" } }, this.state.readyImages.map(function (image, index) { return (react_1.default.createElement("img", { src: image.src, key: index, width: image.clientWidth, height: image.clientHeight, style: {
                    marginLeft: !image.firstRowImage && _this.props.imageMargin && _this.props.imageMargin + "px",
                    marginBottom: _this.props.imageMargin && _this.props.imageMargin + "px"
                } })); }))));
    };
    return LinesGallery;
}(react_1.default.Component));
exports.default = LinesGallery;

lines-gallery
========================
[![npm](https://bytes.nz/b/lines-gallery/npm)](https://npmjs.com/package/lines-gallery)

Image gallery that auto-sizes images to fill and have consistent heights in
each row.

The gallery can either size the images based on the natural dimensions of the
image when it is loaded, or using dimension metadata when it is given
an array of ImageMetadata.

The gallery can also listen for window resize to automatically resize the
images. The gallery hides x overflow to ensure that the containing div does
not get expanded by the images.

```
/**
 * Create a lines gallery in the given element using the given images
 *
 * @param containingelement HTML element to put the gallery in
 * @param images Image to put in the gallery
 * @param options Options
 */
export type linesGallery = (element: HTMLElement,
    images: Array<string | ImageMetadata>, options?: Options) => void;
export default linesGallery;

/**
 * Image metadata to speed up sizing of images and to request an image of a
 * particular size if enabled
 */
interface ImageMetadata {
  /// URL of image
  src: string,
  /// Image width in pixels
  width?: number,
  /// Image height in pixels
  height?: number
}

/**
 * Options that can be passed to linesGallery
 *
 * @see optionDefaults for the default values
 */
interface Options {
  /// Minimum width/height ratio to detect as panaramas
  panaramaRatioMinimum?: number,
  /// Width of gallery
  galleryWidth?: number,
  /// Target number of images per line
  targetImagesPerLine?: number,
  /// Margin between images in pixels
  imageMargin?: number,
  /// Target minimum size of resized images. If an image is larger than this
  /// but will be resized to small than this in either dimension, the number
  /// of images on the row will be adjusted to try and keep the image size
  /// above this value
  minImageSize?: number,
  /// Watch for resize events to resize images when required
  resizeImages?: boolean,
  /**
   * Image URL modification function. Will be called to modify the urls of
   * images before the request is sent to the server
   *
   * @param url Image URL
   * @param width Wanted image width, if have image metadata
   * @param height Wanted image height, if have image metadata
   *
   * @returns Modified URL
   */
  sizedImageRequest? (url: string, width?: number, height?: number): string
}

/**
 * lines-gallery option defaults
 */
export const optionDefaults = {
  panaramaRatioMinimum: 3,
  targetImagesPerLine: 3,
  imageMargin: 10
};

```

# Example
```
  linesGallery(document.getElementById('gallery'), [
    {
      width: 640,
      height: 480,
      src: 'images/example.jpg'
    },
    {
      width: 480,
      height: 640,
      src: 'images/example.jpg'
    },
    {
      width: 640,
      height: 480,
      src: 'images/example.jpg'
    },
    {
      width: 480,
      height: 640,
      src: 'images/example.jpg'
    },
    {
      width: 1500,
      height: 480,
      src: 'images/example.jpg'
    },
    'images/example.jpg',
    'images/example.jpg'
  ], { resizeImages: true });
```

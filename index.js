"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.linesGallery = void 0;
var galleryBuilder_1 = __importDefault(require("./lib/galleryBuilder"));
/**
 * Create a lines gallery in the given element using the given images
 *
 * @param galeryElement HTML element to put the gallery in
 * @param images Image to put in the gallery
 * @param options Options
 */
exports.linesGallery = function (galleryElement, images, givenOptions) {
    if (givenOptions === void 0) { givenOptions = {}; }
    var galleryWidth = givenOptions.galleryWidth || galleryElement.clientWidth;
    var noGalleryWidth = !givenOptions.galleryWidth;
    // Add default values to options
    var options = __assign(__assign({}, givenOptions), { galleryWidth: galleryWidth });
    if (window.getComputedStyle(galleryElement).overflowX !== 'hidden') {
        galleryElement.style.overflowX = 'hidden';
    }
    if (window.getComputedStyle(galleryElement).lineHeight !== '0') {
        galleryElement.style.lineHeight = '0';
    }
    var elements = [];
    var makeImageUrl = function (image) {
        if (options.sizedImageRequest) {
            return options.sizedImageRequest(image.src, image.clientWidth, image.clientHeight);
        }
        else {
            return image.src;
        }
    };
    var updateImages = function (images, firstModifiedImage) {
        var i;
        // Remove any images not in images anymore
        if (elements.length > images.length) {
            for (i = images.length; i < elements.length; i++) {
            }
        }
        // Update any images that have been modified
        for (i = 0; i < elements.length; i++) {
            var src = makeImageUrl(images[i]);
            if (elements[i].src !== src) {
                elements[i].src = src;
            }
            elements[i].width = images[i].clientWidth;
            elements[i].height = images[i].clientHeight;
            if (options.imageMargin) {
                if (images[i].firstRowImage) {
                    elements[i].style.marginTop = '';
                }
                else {
                    elements[i].style.marginTop = options.imageMargin + "px";
                }
                if (images[i].imageFirstInRow) {
                    elements[i].style.marginLeft = '';
                }
                else {
                    elements[i].style.marginLeft = options.imageMargin + "px";
                }
            }
        }
        // Add new images
        for (i; i < images.length; i++) {
            var element = document.createElement('img');
            element.src = makeImageUrl(images[i]);
            element.width = images[i].clientWidth;
            element.height = images[i].clientHeight;
            if (options.imageMargin) {
                if (images[i].firstRowImage) {
                    element.style.marginTop = '';
                }
                else {
                    element.style.marginTop = options.imageMargin + "px";
                }
                if (images[i].imageFirstInRow) {
                    element.style.marginLeft = '';
                }
                else {
                    element.style.marginLeft = options.imageMargin + "px";
                }
            }
            galleryElement.appendChild(element);
            elements.push(element);
        }
    };
    if (!images) {
        // Find images already in element
        images = [];
        elements = galleryElement.querySelectorAll('img');
        if (!elements.length) {
            // No images - do nothing
            return;
        }
        for (var i = 0; i < elements.length; i++) {
            images.push({
                src: elements[i].src,
                width: elements[i].width,
                height: elements[i].height
            });
        }
    }
    var response = galleryBuilder_1.default(images, updateImages, options);
    if (options.resizeImages && noGalleryWidth) {
        window.addEventListener('resize', function () {
            if (galleryElement.clientWidth !== galleryWidth) {
                galleryWidth = galleryElement.clientWidth;
                response.setWidth(galleryWidth);
            }
        });
    }
};
exports.default = exports.linesGallery;
